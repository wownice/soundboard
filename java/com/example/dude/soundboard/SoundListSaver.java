package com.example.dude.soundboard;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SoundListSaver
{
    public static void saveSoundList(Context context, ArrayList<Sound> list, String name, int cols)
    {
        try
        {
            File file = new File(context.getExternalFilesDir(null), name + ".SList");
            file.createNewFile();

            BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));
            writer.write("[NumCol]:" + cols);
            writer.newLine();

            for(Sound s : list)
            {
                writer.write("[Sound]:" + s.getName() + ",,]]" + s.getUri());
                writer.newLine();
            }

            writer.close();
            saveLastOpenedList(context, name);
        } catch (IOException e) {
            Log.e("SoundList", "Unable to save file.");
            e.printStackTrace();
        }
    }

    //fills given soundlist and returns number of columns (should be from first line)
    public static int loadSoundList(Context context, String listName,  ArrayList<Sound> list)
    {
        int cols = -1;
        File file = new File(context.getExternalFilesDir(null), listName + ".SList");
        if (file != null)
        {
            BufferedReader reader = null;
            try
            {
                reader = new BufferedReader(new FileReader(file));
                String line = reader.readLine();
                String[] dataSplit;
                while(line != null)
                {
                    if(line.contains("[Sound]:"))
                    {
                        line = line.replace("[Sound]:", "");
                        dataSplit = line.split(",,]]");
                        list.add(new Sound(dataSplit[0], Uri.parse(dataSplit[1])));
                    }
                    else if(line.contains("[NumCol]:")) {
                        cols = Integer.parseInt(line.replace("[NumCol]:", ""));
                    }
                    line = reader.readLine();
                }
                reader.close();
                saveLastOpenedList(context, listName);
            } catch (Exception e) {
                Log.e("SoundFile", "Unable to read the specified file.");
                e.printStackTrace();
            }
        }
        return cols;
    }

    public static void saveLastOpenedList(Context context, String name)
    {
        try
        {
            File file = new File(context.getExternalFilesDir(null), "opened.last");
            file.createNewFile();

            BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));
            writer.write(name);

            writer.close();
        } catch (IOException e) {
            Log.e("SoundList", "Unable to save last opened file.");
            e.printStackTrace();
        }
    }

    public static int loadLastLoadedList(Context context, ArrayList<Sound> list)
    {
        return loadSoundList(context, getLastOpenedFileName(context), list);
    }

    public static ArrayList<String> getAllSoundLists(Context context)
    {
        ArrayList<String> list = new ArrayList<String>();
        File[] files = context.getExternalFilesDir(null).listFiles();
        String name;
        for(File f : files)
        {
            name = f.getName();
            if(name.contains(".SList"))
            {
                list.add(name.replace(".SList", ""));
            }
        }
        return list;
    }

    public static void deleteSoundList(Context context, String name)
    {
        File deleteFile = new File(context.getExternalFilesDir(null), name + ".SList");
        deleteFile.delete();
    }

    public static boolean listExists(Context context) //returns true if files exist in directory
    {
        File checker = new File(context.getExternalFilesDir(null), "opened.last");
        return checker != null;
    }

    public static String getLastOpenedFileName(Context context)
    {
        File file = new File(context.getExternalFilesDir(null), "opened.last");
        String fileName = "";
        if (file != null)
        {
            BufferedReader reader = null;
            try
            {
                reader = new BufferedReader(new FileReader(file));
                fileName = reader.readLine();
                reader.close();
            }
            catch (Exception e)
            {
                Log.e("SoundFile", "Unable to read the last loaded file.");
                e.printStackTrace();
            }

        }
        else
            Log.e("SoundFile", "No previously loaded list found.");
        return fileName;
    }
}