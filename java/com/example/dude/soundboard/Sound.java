package com.example.dude.soundboard;

import android.net.Uri;

public class Sound
{
    private Uri uri;
    private String soundName;

    private boolean selected; //needed for sound editing

    public Sound(String name, Uri fUri)
    {
        soundName = name;
        uri = fUri;
        selected = false;
    }

    public String getName()
    {
        return soundName;
    }

    public Uri getUri()
    {
        return uri;
    }

    public void updateName(String s)
    {
        soundName = s;
    }

    public void updateBytes(Uri fUri)
    {
        uri = fUri;
    }

    public void toggleSelected()
    {
        selected = !selected;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void deselect()
    {
        selected = false;
    }
}
/*
public class Sound
{
    private byte[] soundBytes;
    private String soundName;

    public Sound(String name, byte[] bytes)
    {
        soundName = name;
        soundBytes = bytes;
    }

    public String getName()
    {
        return soundName;
    }

    public byte[] getBytes()
    {
        return soundBytes;
    }

    public void updateName(String s)
    {
        soundName = s;
    }

    public void updateBytes(byte[] b)
    {
        soundBytes = b;
    }
}

 */