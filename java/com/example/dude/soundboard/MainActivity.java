package com.example.dude.soundboard;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    private Menu boardMenu;
    private ArrayList<Sound> soundsList;
    private GridView grid;
    private ButtonAdapter gridAdapter;
    private SoundPlayer soundPlayer;

    private String boardTitle;
    static int soundManageStatus;
    static final int PLAY_SOUNDS = 1;
    static final int EDIT_SOUNDS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        soundsList = new ArrayList<>();
        soundPlayer = new SoundPlayer(getApplicationContext());
        soundManageStatus = PLAY_SOUNDS;
        setUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)         //right of action bar buttons
    {
        if(soundManageStatus == EDIT_SOUNDS)
        {
            getMenuInflater().inflate(R.menu.editmenu, menu);
        }
        else
        {
            getMenuInflater().inflate(R.menu.mainmenu, menu);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)     //drop down menu on right of action bar
    {
        switch (item.getItemId()) {
            case R.id.newSound:
                openAddSound();
                break;
            case R.id.editSounds:
                editSounds();
                break;
            case R.id.saveSL:
                saveSoundBoard();
                break;
            case R.id.openSL:
                openSoundBoard();
                break;
            case R.id.deleteSL:
                deleteSoundBoard();
                break;
            case R.id.clearBoard:
                clearBoard();
                break;
            case R.id.gridColumn:
                changeGridColumnCount();
                break;
            case R.id.finishEditing:
                returnFromEdit();
                break;
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) //sound loaded
    {
        if(resultCode == RESULT_OK)
        {
            if(requestCode == SoundCreationActivity.SOUND_FILE_RETURN) //file path received
            {
                addSound(data);
            }
            else if(requestCode == SoundCreationActivity.SOUND_EDIT_RETURN)
            {
                editIndividualSound(data);
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        if(soundManageStatus == EDIT_SOUNDS)
        {
            returnFromEdit();
        }
        else
            moveTaskToBack(true);
    }

    public void setUI() {
        grid = findViewById(R.id.soundGrid);
        gridAdapter = new ButtonAdapter(this);
        grid.setAdapter(gridAdapter);
        Button pauseButton = findViewById(R.id.stopButton);
        pauseButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundPlayer.stop();
            }
        });

        if (SoundListSaver.listExists(this)) {
            int numCols = SoundListSaver.loadLastLoadedList(this, soundsList);
            if (numCols > 0) {
                grid.setNumColumns(numCols);
                updateGridView(numCols);
                boardTitle = (SoundListSaver.getLastOpenedFileName(this));
            }
        } else {
            boardTitle = "Unnamed Board";
        }

        setTitle(boardTitle);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (soundManageStatus == PLAY_SOUNDS) {
                    soundPlayer.play(soundsList.get(position));
                } else if (soundManageStatus == EDIT_SOUNDS) {
                    TextView sound = (TextView) view;

                    if (soundsList.get(position).isSelected()) {
                        sound.setTextColor(Color.BLACK);
                    } else {
                        sound.setTextColor(Color.BLUE);
                    }
                    soundsList.get(position).toggleSelected();
                }
            }
        });

        Button editbutton = findViewById(R.id.editButton);
        editbutton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEditIndividualSound();
            }
        });
        Button deletebutton = findViewById(R.id.deleteButton);
        deletebutton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSelectedSounds();
            }
        });
    }

    /*  grid appearance start*/

    public void updateGridView()
    {
        updateGridView(grid.getNumColumns());
    }

    public void updateGridView(int cols) //updates adapter and resizes grid to new list size
    {
        gridAdapter.notifyDataSetChanged();
        resizeGrid(cols);
    }

    public void resizeGrid(int cols) //cols int needed because changing grid cols are not immediately effective
    {   //extends grid height based on item count and column count
        View listItem = gridAdapter.getView(0, null, grid);
        listItem.measure(0, 0);
        int itemHeight = listItem.getMeasuredHeight();

        int x = 1;
        if(soundsList.size() > cols)
        {
            x = soundsList.size() / cols;
            itemHeight *= (x + 1);
        }

        ViewGroup.LayoutParams params = grid.getLayoutParams();
        params.height = itemHeight;
        grid.setLayoutParams(params);
    }

    public void changeGridColumnCount()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Number of Columns (Default: 1)");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                int count = Integer.parseInt(input.getText().toString());
                if(count < 0) {
                    grid.setNumColumns(1);
                    resizeGrid(1);
                }
                else {
                    grid.setNumColumns(count);
                    resizeGrid(count);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void clearBoard() //resets title, list, grid column count
    {
        setTitle("Unnamed Board");
        soundsList = new ArrayList<>();
        updateGridView(1);
    }

    public void toggleEditVisibility() //the bottom bar buttons
    {
        Button stopbutton = findViewById(R.id.stopButton);
        Button editbutton = findViewById(R.id.editButton);
        Button deletebutton = findViewById(R.id.deleteButton);
        if(soundManageStatus == EDIT_SOUNDS)
        {
            stopbutton.setVisibility(View.GONE);

            editbutton.setVisibility(View.VISIBLE);
            deletebutton.setVisibility(View.VISIBLE);
        }
        else
        {
            stopbutton.setVisibility(View.VISIBLE);

            editbutton.setVisibility(View.GONE);
            deletebutton.setVisibility(View.GONE);
        }
    }

    /*  grid appearance end*/

        /*  sound board management  start*/

    public void saveSoundBoard()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sound Board Name");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        input.setText(getTitle());
        input.setSelectAllOnFocus(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String boardName = input.getText().toString();
                SoundListSaver.saveSoundList
                        (getApplicationContext(), soundsList, input.getText().toString(), grid.getNumColumns());
                setTitle(boardName);
                Toast.makeText(getApplicationContext(), boardName + " saved.", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void openSoundBoard()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final ArrayList<String> files = SoundListSaver.getAllSoundLists(this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, files);
        builder.setTitle("Open Board");
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                soundsList = new ArrayList<>();
                String fileName = files.get(which);
                int cols = SoundListSaver.loadSoundList(getApplicationContext(), fileName, soundsList);
                grid.setNumColumns(cols);
                updateGridView(cols);
                setTitle(fileName);
                Toast.makeText(getApplicationContext(), fileName + " loaded.", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void deleteSoundBoard()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final ArrayList<String> files = SoundListSaver.getAllSoundLists(this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, files);
        builder.setTitle("Delete Board");
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String fileName = files.get(which);
                SoundListSaver.deleteSoundList(getApplicationContext(), fileName);
                Toast.makeText(getApplicationContext(), fileName + " deleted.", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        builder.show();
    }

    /*  sound board management  end*/

    /* sound management start*/

    public Sound parseIntent(Intent intent)
    {
        String name = intent.getStringExtra("Name");
        Uri uri = intent.getParcelableExtra("Uri");
        Sound sound = new Sound(name, uri);

        return sound;
    }

    public void openAddSound() //loads up soundcreationactivity
    {
        Intent intent = new Intent(this, SoundCreationActivity.class);
        startActivityForResult(intent, SoundCreationActivity.SOUND_FILE_RETURN);
    }

    public void addSound(Intent intent) // adds sound file returned from soundcreationacitvity
    {
        soundsList.add(parseIntent(intent));
        updateGridView();
    }

    public void editSounds()
    {
        soundManageStatus = EDIT_SOUNDS;
        grid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
        toggleEditVisibility();
        invalidateOptionsMenu();
        setTitle("Edit Sounds");
    }

    public void deselectAllSounds()
    {
        for(Sound s : soundsList)
        {
            s.deselect();
        }

        gridAdapter.notifyDataSetChanged(); //update appearance
    }

    public void returnFromEdit()
    {
        soundManageStatus = PLAY_SOUNDS;
        deselectAllSounds();
        toggleEditVisibility();
        invalidateOptionsMenu();
        setTitle(boardTitle);
    }

    public void deleteSelectedSounds()
    {
        if(soundsList.size() > 0)
        {
            Iterator<Sound> soundIt = soundsList.iterator();

            while (soundIt.hasNext()) {
                Sound s = soundIt.next();
                if (s.isSelected())
                    soundIt.remove();
            }

            updateGridView(); //resize grid
        }
    }

    public int oneSoundSelected() // if one sound is selected, return index for editing
    {
        int index = -1;
        ArrayList<Sound> list = new ArrayList<>();
        for(int i = 0; i < soundsList.size(); i++)
        {
           if(soundsList.get(i).isSelected())
           {
               list.add(soundsList.get(i));
               index = i;
           }
        }

        if(list.size() > 1)
        {
            index = -1;
        }

        return index;
    }

    public void openEditIndividualSound()
    {
        int index = oneSoundSelected();
        if(index >= 0)
        {
            Intent intent = new Intent(this, SoundCreationActivity.class);
            intent.putExtra("Name", soundsList.get(index).getName());
            intent.putExtra("Uri", soundsList.get(index).getUri());
            intent.putExtra("Index", index);
            startActivityForResult(intent, SoundCreationActivity.SOUND_EDIT_RETURN);
        }
    }

    public void editIndividualSound(Intent intent)
    {
        int index =  intent.getIntExtra("Index", -1);
        if(index >= 0)
        {
            soundsList.set(index, parseIntent(intent));
            updateGridView();
        }
    }

    /* sound management end*/

    public class ButtonAdapter extends BaseAdapter
    {
        private Context context;
        private TextView button;

        public ButtonAdapter(Context c) {
            context = c;
        }

        public View getView(int position, View convertView, ViewGroup parent)
        {
            ContextThemeWrapper wrappedContext = new ContextThemeWrapper(context, R.style.soundbutton);
            button = new TextView(wrappedContext, null, 0);
            /*
            if(convertView == null)
            {
                button = new TextView(wrappedContext, null, 0);
            }
            else
            {
                button = (TextView) convertView;
            }
            */

            button.setText(soundsList.get(position).getName());

            return button;
        }

        public int getCount() {
            return soundsList.size();
        }

        public Object getItem(int position) {
            return button;
        }

        public long getItemId(int position) {
            return position;
        }
    }
}



/*
    TO DO
    -height readjustment
    -Reordering
    -Parallel Playing
    -Enabling menu buttons(save,delete) depending on amount of sounds on board
    mainmenu.findItem(R.id.editSound).getIcon().setAlpha(130);
        mainmenu.findItem(R.id.deleteSound).getIcon().setAlpha(130);
        if (!soundsList.isEmpty()) {
            mainmenu.findItem(R.id.editSound).getIcon().setAlpha(255);
            mainmenu.findItem(R.id.deleteSound).getIcon().setAlpha(255);
        }
    -indicate which sound is playing
 */