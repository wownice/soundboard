package com.example.dude.soundboard;

import android.content.Context;
import android.media.MediaPlayer;
import android.widget.Toast;

import java.io.IOException;
public class SoundPlayer
{
    private Context context;
    private MediaPlayer mp;

    public SoundPlayer(Context c)
    {
        context = c;
        mp = new MediaPlayer();
    }

    public void play(Sound s)
    {
        stop();
        try
        {
            mp.setDataSource(context, s.getUri());
            mp.prepare();
            mp.start();
        } catch (IOException e)
        {
            Toast.makeText(context, "File missing or cannot be read.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void stop()
    {
        mp.reset();
    }
}