package com.example.dude.soundboard;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class SoundCreationActivity extends AppCompatActivity
{
    static final int AUDIO_FILE_REQUEST = 10;
    static final int SOUND_FILE_RETURN = 25;
    static final int SOUND_EDIT_RETURN = 35;

    private EditText name;

    private Menu activity_menu; //needed to enable save button and show file added
    private Uri fileUri;

    private int editIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.soundcreation);

        setTitle("New Sound");
        name = findViewById(R.id.soundName);

        //back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button addSoundFileButton = (Button) findViewById(R.id.addSoundFile);
        addSoundFileButton.setOnClickListener(new ImageButton.OnClickListener()
        {
            @Override public void onClick(View v) {browseFile();}
        });

        if(MainActivity.soundManageStatus == MainActivity.EDIT_SOUNDS)
        {
            name.setText(getIntent().getStringExtra("Name"));
            fileUri = getIntent().getParcelableExtra("Uri");
            editIndex = getIntent().getIntExtra("Index", -1);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Intent returnIntent = new Intent(this, MainActivity.class);
        if(item.getItemId() == R.id.saveSound)
        {
            if(fileUri != null)
            {
                if (name.getText() != null && name.getText().toString().trim().length() > 0)
                {
                    returnIntent.putExtra("Name", name.getText().toString());
                }
                else
                {
                    returnIntent.putExtra("Name", "Unnamed Sound");
                }
                returnIntent.putExtra("Uri", fileUri);

                if(MainActivity.soundManageStatus == MainActivity.EDIT_SOUNDS)
                {
                    returnIntent.putExtra("Index", editIndex);
                }

            }
            setResult(RESULT_OK, returnIntent);
        }

        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)   //loads save button
    {
        activity_menu = menu;
        getMenuInflater().inflate(R.menu.newsoundmenu, menu);

        if(MainActivity.soundManageStatus == MainActivity.EDIT_SOUNDS)
        {
            enableContinue();
        }

        return true;
    }

    public void browseFile()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("audio/*");
        Intent i = Intent.createChooser(intent, "File");
        startActivityForResult(i, AUDIO_FILE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        /*
            gets selected sound name
            sets textview to sound name
            updates actionbar save button to be enabled
         */

        if (data != null && requestCode == AUDIO_FILE_REQUEST)
        {// data != null needed in case the back button is pressed when browsing files
            fileUri = data.getData();
            enableContinue();
        }
    }

    public void enableContinue() //when sound name and file is selected
    {
        activity_menu.findItem(R.id.saveSound).setEnabled(true);
        findViewById(R.id.fileAddStatus).setVisibility(View.VISIBLE);
    }
}